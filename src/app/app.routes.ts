import { Routes } from '@angular/router';
import { LoginPageComponent } from './modules/auth/page/login-page/login-page.component';
import { HomeComponent } from './modules/valuation/pages/home/home/home.component';
import { CreateEvaluationComponent } from './modules/valuation/pages/createEvaluation/create-evaluation/create-evaluation.component';
import { EvaluationLayoutComponent } from './modules/valuation/layout/evaluation-layout/evaluation-layout.component';

export const routes: Routes = [
    {
        path: "login",
        component: LoginPageComponent
    },
    
    {
        path: '',
        component: EvaluationLayoutComponent,
        children: [
            {
                path: 'home',
                component: HomeComponent,
                data: { title: 'Home' }
            },
            {
                path: 'create-evaluation',
                component: CreateEvaluationComponent,
                data: { title: 'Create Evaluation' }
            },
            {
                path: '',
                redirectTo: '/home',
                pathMatch: 'full'
            }
        ]
    },
    
    {
        path: "**",
        redirectTo: "/login",
        pathMatch: "full"
    }
];
