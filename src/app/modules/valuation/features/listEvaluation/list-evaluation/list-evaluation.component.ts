import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { EvaluationService } from '../../../services/evaluation/evaluation.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpClient, HttpClientModule } from '@angular/common/http';


@Component({
  selector: 'app-list-evaluation',
  standalone: true,
  imports: [MatTableModule, MatButtonModule, MatIconModule, CommonModule, HttpClientModule],
  providers: [EvaluationService],
  templateUrl: './list-evaluation.component.html',
  styleUrl: './list-evaluation.component.scss'
})
export class ListEvaluationComponent {

  displayedColumns: string[] = ['reference', 'receivedOn', 'borrowerName', 'fosRef', 'createdOn', 'modifiedOn', 'action'];
  
  dataSource: any[] = [];

  isLoading = true;

  constructor(private dataService: EvaluationService, private snackBar: MatSnackBar) { }

  ngOnInit(): void {

  this.dataService.getEvaluationList().subscribe(
    data => {

      // TODO: map données reçu si pas de même format que la liste en bas
      this.dataSource = data;
      this.isLoading = false;
    },
    error => {
      this.isLoading = false;

      //TODO: Enlever les données de test
      this.dataSource = ELEMENT_DATA;

      this.snackBar.open('Error loading data', 'Close', {
        duration: 5000,
      });
    }
  );

  }

}

const ELEMENT_DATA: any[] = [
  {
    reference: 'REF001',
    receivedOn: new Date(),
    borrowerName: 'John Doe',
    fosRef: 'FOS001',
    createdOn: new Date(),
    modifiedOn: new Date()
  },
  {
    reference: 'REF001',
    receivedOn: new Date(),
    borrowerName: 'John Doe',
    fosRef: 'FOS001',
    createdOn: new Date(),
    modifiedOn: new Date()
  },
  {
    reference: 'REF001',
    receivedOn: new Date(),
    borrowerName: 'John Doe',
    fosRef: 'FOS001',
    createdOn: new Date(),
    modifiedOn: new Date()
  },
  {
    reference: 'REF001',
    receivedOn: new Date(),
    borrowerName: 'John Doe',
    fosRef: 'FOS001',
    createdOn: new Date(),
    modifiedOn: new Date()
  },
  {
    reference: 'REF001',
    receivedOn: new Date(),
    borrowerName: 'John Doe',
    fosRef: 'FOS001',
    createdOn: new Date(),
    modifiedOn: new Date()
  },
  {
    reference: 'REF001',
    receivedOn: new Date(),
    borrowerName: 'John Doe',
    fosRef: 'FOS001',
    createdOn: new Date(),
    modifiedOn: new Date()
  },
  {
    reference: 'REF001',
    receivedOn: new Date(),
    borrowerName: 'John Doe',
    fosRef: 'FOS001',
    createdOn: new Date(),
    modifiedOn: new Date()
  },
  {
    reference: 'REF001',
    receivedOn: new Date(),
    borrowerName: 'John Doe',
    fosRef: 'FOS001',
    createdOn: new Date(),
    modifiedOn: new Date()
  },
  {
    reference: 'REF001',
    receivedOn: new Date(),
    borrowerName: 'John Doe',
    fosRef: 'FOS001',
    createdOn: new Date(),
    modifiedOn: new Date()
  },
];