import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root',

})
export class EvaluationService {

  constructor(private http: HttpClient) { }

  getEvaluationList(): Observable<any> {

    // TODO: changer le lien de l'API
    return this.http.get('https://api.example.com/data');

  }
  
}
