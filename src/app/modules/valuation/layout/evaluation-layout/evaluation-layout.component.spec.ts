import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EvaluationLayoutComponent } from './evaluation-layout.component';

describe('EvaluationLayoutComponent', () => {
  let component: EvaluationLayoutComponent;
  let fixture: ComponentFixture<EvaluationLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [EvaluationLayoutComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EvaluationLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
