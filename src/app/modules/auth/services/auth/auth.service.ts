import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  //TODO: Remplacez cette URL par l'URL de votre API
  private apiUrl = 'http://your-api-url.com'; // API Login ici

  constructor(private http: HttpClient) { }

  login(credentials: {username?: string | null, password?: string | null}): Observable<any> {

    //TODO: map input api si nécessaire
    return this.http.post(`${this.apiUrl}/login`, credentials);
  }

}